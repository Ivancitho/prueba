package ejercicio01previo2;

import java.util.Scanner;

public class Ejercicio01Previo2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame una opcion(numero)");
		int numero1 = input.nextInt();
		System.out.println("Dame una opcion(numero)");
		int numero2 = input.nextInt();
		
		System.out.println("1-Suma");
		System.out.println("2-Resta");
		System.out.println("3-Multiplicacion");
		System.out.println("4-Division");
		System.out.println("Dame una opcion");
		int opcion = input.nextInt();
		
		switch (opcion)
		{
		case 1: 
			System.out.println("Has elegido opcion 1 " +(numero1 + numero2));
			break;
		
		case 2:
			System.out.println("Has elegido opcion 2 " +(numero1 - numero2));
			break;
		case 3:
			System.out.println("Has elegido opcion 3 " +(numero1 * numero2));
			break;
		case 4:
			System.out.println("Has elegido opcion 4 " +(numero1 / numero2));
			break;
		default:
			System.out.println("No has pulsado ninguna de las opciones elegidas");
		}
		
		input.close();


	}

}
