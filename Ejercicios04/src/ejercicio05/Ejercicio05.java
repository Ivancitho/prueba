package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
	Scanner lector = new Scanner(System.in);
	
	System.out.println("Introduce un mes");
	String mes = lector.nextLine();
	mes = mes.toLowerCase();
	
	switch (mes)
	{
	case "enero":
	case "marzo":
	case "mayo":
	case "julio":
	case "agosto":
	case "octubre":
	case "diciembre":
		System.out.println("El mes tiene 31 dias");
		break;
	case "abril":
	case "junio":
	case "septiembre":
	case "noviembre":
		System.out.println("El mes tiene 30 dias");
		break;
	case "febrero":
		System.out.println("El mes tiene 28 dias");
		break;
	default:
		System.out.println("Mes no valido");
	}
	
	lector.close();

	}

}
