package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
	Scanner lector = new Scanner(System.in);
	
	System.out.println("Caso 1 Viva España");
	
	System.out.println("Introduce un caracter en minuscula");
	char letra = lector.nextLine().charAt(0);
	
	switch (letra)
	{
	case 'a':
		System.out.println("La vocal es "+letra);
		break;
	case 'e':
		System.out.println("La vocal es "+letra);
		break;
	case 'i':
		System.out.println("La vocal es "+letra);
		break;
	case 'o':
		System.out.println("La vocal es "+letra);
		break;
	case 'u':
		System.out.println("La vocal es "+letra);
		break;
	default:
		System.out.println("No es una vocal "+letra);
	}
	
	System.out.println("Caso 1.2");
	
	System.out.println("Introduce un caracter en minuscula");
	char letra2 = lector.nextLine().charAt(0);
	
	switch (letra2)
	{
	case 97:
		System.out.println("La vocal es "+letra2);
		break;
	case 101:
		System.out.println("La vocal es "+letra2);
		break;
	case 105:
		System.out.println("La vocal es "+letra2);
		break;
	case 'o':
		System.out.println("La vocal es "+letra2);
		break;
	case 'u':
		System.out.println("La vocal es "+letra2);
		break;
	default:
		System.out.println("No es una vocal "+letra2);
	}
	
	System.out.println("Caso 2");
	
	System.out.println("Introduce un caracter en minuscula");
	char letra1 = lector.nextLine().charAt(0);
	
	switch (letra1)
	{
	case 'a':
		System.out.println("La vocal es "+letra1);
	case 'e':
		System.out.println("La vocal es "+letra1);
	case 'i':
		System.out.println("La vocal es "+letra1);
	case 'o':
		System.out.println("La vocal es "+letra1);
	case 'u':
		System.out.println("La vocal es "+letra1);
	default:
		System.out.println("No es una vocal "+letra1);
	}
	
	System.out.println("Si no ponemos el break, nos salen los siguientes textos");
	
	lector.close();

	}

}
